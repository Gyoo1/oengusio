export const environment = {
  production: true,
  sandbox: true,
  baseSite: 'https://sandbox.oengus.io',
  api: 'https://sandbox.oengus.io/api',
  twitchLoginClientId: 'yvlsv2i2pf9euy7jy9rarqlnlccagn',
  twitchSyncClientId: 'tawaaqlw4gmwf0c2w5mfr4w2y8sx8b',
  loginRedirect: 'https://sandbox.oengus.io/login/',
  syncRedirect: 'https://sandbox.oengus.io/user/settings/sync/',
  matomoId: 2,
  paypalClientId: 'Ac7rzLgpb5emA9JuRxRXpRDVMdULzgA_BxwyhPlAxPHtg1NtDv3nyjLcWgHBOUEmtdWJ5npWnMN-b7_8',
  closingDate: new Date(2021, 6, 1, 0, 0, 0)
};
